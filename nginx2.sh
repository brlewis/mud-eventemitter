#!/bin/sh
# https://gist.github.com/cecilemuller/a26737699a7e70a7093d4dc115915de8
# To be run after a certificate has been obtained with nginx1.sh

if [ "x$1" = "x" ]; then
    echo "Usage: sudo $0 www.mydomain.com"
    exit 1
fi
host="$1"
domain=$(echo "$host" | sed 's/^[^\.]*\.//')

echo Must run as root...

cat > /etc/nginx/sites-available/${domain}.conf <<EOF
## http://${domain} redirects to https://${domain}
## For compatibility with HSTS, don't go directly to https://${host}
server {
	listen 80;
	listen [::]:80;
	server_name ${domain};

	include /etc/nginx/snippets/letsencrypt.conf;

	location / {
		return 301 https://${domain}\$request_uri;
	}
}

## http://${host} redirects to https://${host}
server {
	listen 80;
	listen [::]:80;
	server_name ${host};

	include /etc/nginx/snippets/letsencrypt.conf;

	location / {
		return 301 https://${host}\$request_uri;
	}
}

## https://${domain} redirects to https://${host}
server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;
	server_name ${domain};

	ssl_certificate /etc/letsencrypt/live/${host}/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/${host}/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/${host}/fullchain.pem;
	include /etc/nginx/snippets/ssl.conf;

	location / {
		return 301 https://${host}\$request_uri;
	}
}

## Serves https://${host}
server {
	server_name ${host};
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	ssl_certificate /etc/letsencrypt/live/${host}/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/${host}/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/${host}/fullchain.pem;
	include /etc/nginx/snippets/ssl.conf;

	root /var/www/${host};
	index index.html;
	location / {
		try_files \$uri \$uri/ =404;
	}
	location /git-pull {
		proxy_pass http://127.0.0.1:3000;
	}
	location /socket {
		proxy_pass http://127.0.0.1:3001;
                proxy_http_version 1.1;
                proxy_read_timeout 1800;
                proxy_set_header Upgrade \$http_upgrade;
                proxy_set_header Connection "upgrade";
	}
}
EOF

systemctl reload nginx

hook=/root/letsencrypt.sh

if [ ! -x ${hook} ]; then
    cat > ${hook} <<EOF
#!/bin/bash
systemctl reload nginx

# If you have other services that use the certificates:
# systemctl restart mosquitto
EOF
    chmod 755 ${hook}
fi

(crontab -l | grep -v ${hook}; echo "52 6 * * * certbot -q renew --noninteractive --renew-hook ${hook}") | crontab
