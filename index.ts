import * as http from 'http';
import * as WebSocket from 'ws';
import { exec } from 'child_process';
import { FrontDeskArea } from './src/FrontDesk';

const hostname = '127.0.0.1';
const port = 3000;
const wport = 3001;

function gitPull(req, res) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    // relying on the forever script to pull, etc.
    res.end('ok');
    server.close(error => process.exit());
}

function safeValue(val) {
    if (val === null || ['boolean', 'string', 'number'].includes(typeof val)) {
        return val;
    } else {
        return typeof val;
    }
}

function typeValues(obj) {
    const newObj = {};
    Object.keys(obj).forEach(key => newObj[key] = safeValue(obj[key]));
    return newObj;
}

function nonRecursive(key, val) {
    if (!key) {
        return val;
    }
    if (val !== null && typeof val === 'object') {
        return typeValues(val);
    } else {
        return val;
    }
}

function echoReq(req, res) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(JSON.stringify(req, nonRecursive, 4));
}

const server = http.createServer((req, res) => {
    if (/git-pull/.test(req.url)) {
        gitPull(req, res);
    } else {
        echoReq(req, res);
    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

const wss = new WebSocket.Server({
    host: hostname,
    port: wport
});

const desk = new FrontDeskArea();

wss.on('connection', function connection(ws) {
    ws.on('error', function(e) {
        console.log(e);
    });
    desk.play(ws);
});
