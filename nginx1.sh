#!/bin/sh
# https://gist.github.com/cecilemuller/a26737699a7e70a7093d4dc115915de8

if [ "x$1" = "x" ]; then
    echo "Usage: sudo $0 www.mydomain.com"
    exit 1
fi
host="$1"
domain=$(echo "$host" | sed 's/^[^\.]*\.//')

echo Must run as root...

apt install nginx
cat > /etc/nginx/snippets/letsencrypt.conf <<EOF
location ^~ /.well-known/acme-challenge/ {
	default_type "text/plain";
	root /var/www/letsencrypt;
}
EOF
cat > /etc/nginx/snippets/ssl.conf <<EOF
ssl_session_timeout 1d;
ssl_session_cache shared:SSL:50m;
ssl_session_tickets off;

ssl_protocols TLSv1.2;
ssl_ciphers EECDH+AESGCM:EECDH+AES;
ssl_ecdh_curve secp384r1;
ssl_prefer_server_ciphers on;

ssl_stapling on;
ssl_stapling_verify on;

add_header Strict-Transport-Security "max-age=15768000; includeSubdomains; preload";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
EOF
mkdir -p /var/www/letsencrypt/.well-known/acme-challenge
cat > /etc/nginx/sites-available/${domain}.conf <<EOF
server {
	listen 80;
	listen [::]:80;
	server_name ${domain} ${host};

	include /etc/nginx/snippets/letsencrypt.conf;

	root /var/www/${domain};
	index index.html;
	location / {
		try_files \$uri \$uri/ =404;
	}
}
EOF
rm -f /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/${domain}.conf /etc/nginx/sites-enabled/${domain}.conf
systemctl reload nginx
apt-get install software-properties-common
add-apt-repository ppa:certbot/certbot
apt-get update
apt-get install certbot
certbot certonly --webroot --agree-tos --no-eff-email --email ourdoings@gmail.com -w /var/www/letsencrypt -d ${host}
