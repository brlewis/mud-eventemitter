import { Mountain } from './src/Mountain';
import Yeller from './src/Yeller';
import NodeClient from './src/NodeClient';

new Yeller(new NodeClient());
new Mountain();
