import * as events from 'events';
import Client from './Client';

export default class NullClient extends events.EventEmitter implements Client {
    send(message: string) {
    };
    terminate() {
    }
}
