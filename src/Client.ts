import * as readline from 'readline';
import * as events from 'events';

export default interface Client {
    send(message: string, handler?: Function): void;
    on(name: 'message', listener: (message: string) => void);
    emit(name: 'message', command: string);
    terminate();
}
