import * as readline from 'readline';
import * as events from 'events';
import Client from './Client';
import * as wrap from 'wordwrap';

export default class NodeClient extends events.EventEmitter implements Client {
    constructor() {
        super();
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        rl.on('line', input => this.emit('message', input));
    };
    send(message: string) {
        if (message) {
            console.log(wrap(process.stdout.columns)(message));
        } else {
            console.log('debug: ' + message);
        }
    };
    terminate() {
    };
}
