import * as n from './Narrator';
import Client from './Client';
import { Mountain } from './Mountain';

class InitiationForm {
    player: n.Body;
    named: boolean = false;
    spoke: boolean = false;
    emoted: boolean = false;
    constructor(player: n.Body) {
        this.player = player;
    };
    ready(): boolean {
        return this.named && this.spoke && this.emoted;
    };
    hints(): Array<string> {
        if (!this.named) {
            return ['say Use the name command to choose your name.'];
        } else if (!this.spoke) {
            return ['say Use the say command to say anything.'];
        } else if (!this.emoted) {
            return ['say Use the emote command for any social gesture. Do this...',
                'emote smiles.'];
        }
    };
}

class DeskWorker extends n.Automaton {
    names = ['worker'];
    destination: n.Room;
    forms: Array<InitiationForm> = [];
    eject(victim: n.Body): void {
        this.plan(1, () => {
            this.cmd(`emote presses a button and ${victim.properName} instantly disappears.`);
            victim.enter(this.destination);
        });
    };
    giveHint(n: number): Function {
        return () => {
            const hints = this.forms[n].hints();
            if (hints) {
                hints.forEach(message => this.cmd(message));
                this.plan(30, () => this.cmd('emote waits patiently.'));
            } else {
                const playerName = this.forms[n].player.properName;
                this.cmd(`emote congratulates ${playerName} on a job well done.`)
                this.plan(1, () => {
                    this.cmd('say There are many more commands. There are directions like north, south, east, west. These can be shortened to n, s, e, w. There\'s a ‘stuck’ command that brings you back here when you get stuck. And there are others, but you\'ll figure them out as you go along.');
                    this.eject(this.forms[n].player);
                });
            }
        };
    };
    properName = 'a desk worker';
    description(): string { return 'The desk worker is wearing a bright, yellow polo shirt with “Evolving Story” embroidered on it. His hair is neatly combed.'; };
    fidget() {
        this.cmd('emote fidgets.');
        this.plan(10, this.fidget);
    };
    reset() {
        this.plan(4, this.fidget);
    };
    constructor(location: n.Entity) {
        super(location);
        this.on('enter', this.seeEnter.bind(this));
        this.on('name', this.seeName.bind(this));
        this.on('say', this.seeSay.bind(this));
        this.on('emote', this.seeEmote.bind(this));
    };
    seeEnter(act: n.ActionResult): void {
        if (act.initiator instanceof n.Body) {
            for (let i = 0; i < this.forms.length; i++) {
                if (this.forms[i].player === act.initiator) {
                    this.plan(1, () => {
                        if (act.initiator instanceof n.Body) {
                            this.cmd(`emote looks at ${act.initiator.properName} and smirks.`);
                            this.eject(act.initiator);
                        }
                    });
                    return;
                }
            }
            this.forms.push(new InitiationForm(act.initiator));
            this.plan(2, () => {
                this.cmd('emote pulls a form out of a drawer.');
                this.plan(2, this.giveHint(this.forms.length - 1));
            });
        }
    };
    seeName(act: n.NameResult): void {
        if (act.initiator instanceof n.Body) {
            for (let i = 0; i < this.forms.length; i++) {
                if (this.forms[i].player === act.initiator) {
                    this.forms[i].named = true;
                    this.cmd(`emote writes “${act.name}” on a form.`);
                    this.plan(2, this.giveHint(i));
                    break;
                }
            }
        }
    };
    seeSay(act: n.SayResult): void {
        if (act.initiator instanceof n.Body) {
            for (let i = 0; i < this.forms.length; i++) {
                if (this.forms[i].player === act.initiator) {
                    this.forms[i].spoke = true;
                    this.cmd('emote checks a box on a form.');
                    this.plan(2, this.giveHint(i));
                    break;
                }
            }
        }
    };
    seeEmote(act: n.EmoteResult): void {
        if (act.initiator instanceof n.Body) {
            for (let i = 0; i < this.forms.length; i++) {
                if (this.forms[i].player === act.initiator) {
                    if (/^smiles\.?$/.test(act.emote)) {
                        this.cmd('emote checks a box on a form.');
                        this.forms[i].emoted = true;
                    } else {
                        this.cmd('say Try to do exactly what I do.');
                    }
                    this.plan(2, this.giveHint(i));
                    break;
                }
            }
        }
    };
}

class FrontDesk extends n.Room {
    description(observer?: n.Entity) {
        return 'You are in a bright, tidy front desk area. ' + this.contentDescription(observer);
    };
}

export class FrontDeskArea {
    frontDesk: FrontDesk;
    deskWorker: DeskWorker;
    mountain: Mountain;
    play(client: Client): n.Body {
        return new n.Body(this.frontDesk, client);
    };
    constructor() {
        this.mountain = new Mountain();
        this.frontDesk = new FrontDesk();
        this.frontDesk.reset();
        this.deskWorker = new DeskWorker(this.frontDesk);
        this.deskWorker.reset();
        this.deskWorker.destination = this.mountain.start;
    }
}
