import { Entity, Body, Room, Item, North, South, East, West } from './Narrator';
import Client from './Client';

class Place extends Room {
    desc: string;
    area: BottomofMountain;
    constructor(area: BottomofMountain) {
        super();
        this.area = area;
    };
    description(observer?: Entity) {
        return this.desc + ' ' + this.contentDescription(observer);
    };
}

class Forest extends Place {
    reset() {
        this.contents = [
            new East(this, this.area.village),
        ];
    }
    desc: string = 'You find yourself at the bottom of the mountain. To the West is an impenetrable dark forest, you hear birds and rustling leaves. To the East you see a small village with smoke coming out of the chimneys and children playing outside. To the North you see a lake with sailboats. Where do you go?';
}

class Village extends Place {
    reset() {
        this.contents = [
            new North(this, this.area.outsideoftown),
            new West(this, this.area.forest)
        ];
        this.desc = 'You are in the village and all the people are looking at you. You hear your name and a mysterious old woman beckons for you from inside a dark room. To the North is a horse already saddled up. Nearby there are men with pistols drinking beer and singing loudly. Where do you go?'
    }
}
class OutsideofTown extends Place {
    reset() {
        this.contents = [
            new South(this, this.area.village),
            new West(this, this.area.steamboat)
        ];
        this.desc = 'You are riding the horse out of the town. To the West is the lake with sailboats you’d see earlier. To the North you see a pack of wolves. To the South you see the village you came from. Where do you go?'
    }
}
class Steamboat extends Place {
    reset() {
        this.contents = [
            new East(this, this.area.outsideoftown),
            new West(this, this.area.docks)
        ];
        this.desc = 'You and your horse have boarded a steamboat on the lake. To the North is a waterfall. To the South is a deserted island. To the West are docks with steamboats letting passengers on and off. Where do you go? '
    }
}
class Docks extends Place {
    reset() {
        this.contents = [
            new North(this, this.area.inn),
            new East(this, this.area.steamboat)
        ];
        this.desc = 'You are moored up to the docks, you and your house get off and look around. To the West is a market with people selling supplies. To the West is a tall wall. To the North you see a warmly lit Inn with the smell of food wafting out. Where do you go?'
    }
}
class Inn extends Place {
    reset() {
        this.contents = [
            new North(this, this.area.horsetrader),
            new South(this, this.area.docks)
        ];
        this.desc = 'In the Inn you sit down for a bite to eat and realize you don’t have any money. To the North is a man with a sign, Will Buy Horses. To the West is the Sheriff. To the South is the exit. Where do you go?'
    }
}
class HorseTrader extends Place {
    reset() {
        this.contents = [
            new South(this, this.area.inn)
        ];
        this.desc = 'You are talking to the man who buys horses, you argue with him over the price and sell your horse for a good amount of money. You buy a meal to go and exit the Inn. To the South you see the market. To the North you see a train station. To the West you see a farm. Where do you go?'
    }
}

class Seeds extends Item {
    names = ['seeds'];
    oneLiner() {
        return 'some seeds';
    };
}



export class BottomofMountain {
    forest: Place;
    village: Place;
    outsideoftown: Place;
    steamboat: Place;
    docks: Place;
    inn: Place;
    horsetrader: Place;
    play(client: Client): Body {
        return new Body(this.forest, client);
    };
    constructor() {
        this.forest = new Forest(this);
        this.village = new Village(this);
        this.outsideoftown = new OutsideofTown(this);
        this.steamboat = new Steamboat(this);
        this.docks = new Docks(this);
        this.inn = new Inn(this);
        this.horsetrader = new HorseTrader(this);

        Object.keys(this).forEach(key => {
            const member = this[key];
            if (member instanceof Place) {
                member.reset();
            }
        });
    }
}
