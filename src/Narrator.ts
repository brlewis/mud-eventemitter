import * as events from 'events';
import Client from './Client';
import NullClient from './NullClient';

export const cap = (str: string) => str[0].toUpperCase() + str.substr(1);

export class Entity extends events.EventEmitter {
    location?: Entity;
    enter(location: Entity, theExit?: Exit): void {
        if (this.location) {
            this.location.contents = this.location.contents.filter(
                (e: Entity) => e !== this);
        }
        this.location = location;
        location.contents.push(this);
    };
    contents: Array<Entity> = [];
    has(f: (value: Entity, index: number, array: Entity[]) => any): number {
        return this.contents.filter(f).length;
    };
    contentString(observer?: Entity): string {
        const contents = this.contents.filter(
            (e: Entity) => e !== observer && e.oneLiner(observer));
        const len = contents.length;
        if (len === 0) {
            return '';
        }
        if (len === 1) {
            return contents[0].oneLiner(observer);
        }
        let str = contents[0].oneLiner(observer);
        for (let i = 1; i < len - 1; i++) {
            str += ', ' + contents[i].oneLiner(observer);
        }
        return str + ' and ' + contents[len - 1].oneLiner(observer);
    };
    names: Array<string>;
    oneLiner(observer?: Entity): string {
        return '';
    };
    description(observer?: Entity): string {
        return this.oneLiner(observer)
    };
    actions: any = {};
    addAction(action: Action): void {
        action.verb.forEach(verb => this.actions[verb] = action);
    }
    cmd(full: string): boolean {
        return this.handleCommand(this, full, full.split(' '));
    };
    handleCommand(initiator: Entity, full: string, words: Array<string>): boolean {
        const action = this.actions[words[0].toLowerCase()];
        if (action && (words.length === 2 && this.names && this.names.indexOf(words[1].toLowerCase()) >= 0 ||
            words.length === 1 && initiator === this)) {
            action.attempt(initiator, [this]);
            return true;
        }
        return false;
    };
    initialLocation: Entity;
    repeatLocationEvents: boolean = false;
    reset(): void {
        if (this.initialLocation && this.initialLocation !== this.location) {
            this.enter(this.initialLocation);
        }
    };
    constructor(location?: Entity) {
        super();
        this.addAction(new Look());
        if (location) {
            this.initialLocation = location;
            this.enter(location);
        }
    };
};

export class ActionResult {
    initiator: Entity;
    receivers: Array<Entity>;
    success: boolean;
    constructor(initiator: Entity, receivers: Array<Entity>, success: boolean) {
        this.initiator = initiator;
        this.receivers = receivers;
        this.success = success;
    }
}

export class Action {
    verb: Array<string>;
    attempt(initiator: Entity, receivers: Array<Entity>): void {
        this.succeed(initiator, receivers);
    };
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        initiator.location.emit(this.verb[0], {
            initiator, receivers, success: true
        });
    };
};

export class Look extends Action {
    verb: Array<string> = ['look', 'examine', 'exa'];
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        if (initiator instanceof Body && initiator.location === receivers[0].location) {
            if (receivers[0] instanceof Exit) {
                initiator.location.emit('message', `${initiator.properName} looks ${receivers[0].names[0]}.`);
            } else {
                initiator.location.emit('message', `${cap(initiator.properName)} looks at ${receivers[0].oneLiner()}.`);
            }
            initiator.emit('message', receivers[0].description(initiator));
        }
        super.succeed(initiator, receivers);
    };
}

export class Take extends Action {
    verb: Array<string> = ['take', 'get', 'grab'];
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        if (initiator instanceof Body && initiator.location === receivers[0].location) {
            // FIXME: appropriate message if wrong location
            receivers[0].enter(initiator);
            initiator.location.emit('message', `${initiator.properName} gets ${receivers[0].oneLiner()}.`);
        }
        super.succeed(initiator, receivers);
    };
}

export class Drop extends Action {
    verb: Array<string> = ['drop'];
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        if (initiator instanceof Body && receivers[0].location === initiator) {
            // FIXME: appropriate message if wrong location
            receivers[0].enter(initiator.location);
            initiator.location.emit('message', `${initiator.properName} drops ${receivers[0].oneLiner()}`);
        }
        super.succeed(initiator, receivers);
    };
}

export class Item extends Entity {
    constructor(location: Entity) {
        super(location);
        this.addAction(new Take());
        this.addAction(new Drop());
    };
}

export class Go extends Action {
    verb: Array<string> = ['go', 'walk'];
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        super.succeed(initiator, receivers);
        const theExit = receivers[0];
        if (theExit instanceof Exit) {
            const oldLocation = initiator.location;
            initiator.enter(theExit.destination, theExit);
            if (initiator instanceof Body) {
                oldLocation.emit('message', `${cap(initiator.properName)} ${initiator.goes} ${theExit.names[0]}.`);
            }
        };
    }
};

export class Exit extends Entity {
    destination: Entity;
    description(): string { return 'It looks like you could go that way.'; };
    constructor(location: Entity, destination: Entity) {
        super(location);
        this.destination = destination;
        this.addAction(new Go());
    };
    handleCommand(initiator: Entity, full: string, words: Array<string>): boolean {
        if (words.length === 2) {
            return super.handleCommand(initiator, full, words);
        }

        if (words.length === 1 && this.names.indexOf(words[0].toLowerCase()) >= 0) {
            this.actions.go.attempt(initiator, [this]);
            return true;
        }
        return false;
    };
}

export class North extends Exit {
    names: Array<string> = ['north', 'n'];
}

export class South extends Exit {
    names: Array<string> = ['south', 's'];
}

export class East extends Exit {
    names: Array<string> = ['east', 'e'];
}

export class West extends Exit {
    names: Array<string> = ['west', 'w'];
}

export class Up extends Exit {
    names: Array<string> = ['up', 'u'];
}

export class Down extends Exit {
    names: Array<string> = ['down', 'd'];
}

export class Stuck extends Action {
    verb: Array<string> = ['stuck'];
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        super.succeed(initiator, receivers);
        if (initiator instanceof Body) {
            initiator.location.emit('message',
                cap(`${initiator.properName} slowly fades from existence.`));
            initiator.contents.forEach(entity => entity.reset());
            initiator.reset();
        }
    };
}

export class NameResult extends ActionResult {
    name: string;
    constructor(initiator: Entity, name: string) {
        super(initiator, [], true);
        this.name = name;
    }
}

export class SayResult extends ActionResult {
    message: string;
    constructor(initiator: Entity, message: string) {
        super(initiator, [], true);
        this.message = message;
    }
}

export class EmoteResult extends ActionResult {
    emote: string;
    constructor(initiator: Entity, emote: string) {
        super(initiator, [], true);
        this.emote = emote;
    }
}

export class Body extends Entity {
    error?: Error;
    properName: string = 'somebody';
    oneLiner(observer?: Entity) {
        return this.properName;
    };
    client: Client;
    send(message: string): void {
        if (!this.error) {
            this.client.send(message, (error: Error) => {
                if (error) {
                    this.error = error;
                    console.error('client send error: ' + error.message);
                    this.location.removeListener('message', this.sender);
                    this.cmd('quit');
                    return;
                }
            });
        }
    };
    sender: (...args: any[]) => void;
    goes: string = 'goes';
    cmd(command: string): boolean {
        const words = command.split(' ');
        switch (words[0].toLowerCase()) {
            case 'quit':
                this.location.emit('message', cap(`${this.properName} abruptly disappears.`));
                this.location.emit('quit', new ActionResult(this, [], true));
                this.contents.forEach(item => item.reset());
                this.location.contents = this.location.contents.filter(
                    (e: Entity) => e !== this);
                this.location = null;
                this.client.terminate();
                return;
            case 'say':
                const msg = words.slice(1).join(' ');
                this.location.emit('message', cap(`${this.properName} says, “${msg}”`));
                this.location.emit('say', new SayResult(this, msg));
                return;
            case 'emote':
                let emote = words.slice(1).join(' ');
                if (/[a-z]$/i.test(emote)) {
                    emote += '.';
                }
                this.location.emit('message', cap(`${this.properName} ${emote}`));
                this.location.emit('emote', new EmoteResult(this, emote));
                return;
            case 'inventory':
            case 'inv':
            case 'i':
                this.send(`You have ${this.contentString(this) || 'nothing'}.`);
                return;
            case 'name':
                const properName = words.slice(1).join(' ');
                if (/^[a-z]/i.test(properName)) {
                    this.location.emit('message',
                        cap(`${this.properName} is now named ${properName}.`));
                    this.properName = properName;
                    this.location.emit('name', new NameResult(this, properName));
                } else {
                    this.client.send('Your name must start with a letter.');
                }
                return;
            default:
                this.contents.find(item => item.handleCommand(this, command, words)) ||
                    this.location.handleCommand(this, command, words) ||
                    this.handleCommand(this, command, words) ||
                    this.send('You cannot do that now.');
        }
    };

    constructor(location: Entity, client: Client) {
        super();
        this.sender = this.send.bind(this);
        this.client = client;
        client.on('message', this.cmd.bind(this));
        this.repeatLocationEvents = true;
        this.initialLocation = location;
        this.enter(location);
        this.on('message', this.sender);
        this.addAction(new Stuck());
    };
    enter(location: Entity) {
        location.emit('message', cap(`${this.properName} enters.`));
        super.enter(location);
        this.send(location.description(this));
        this.location.emit('enter', {
            initiator: this,
            receivers: [],
            success: true
        });
    };
}

export class Automaton extends Body {
    timer: any;
    plan(seconds: number, f: Function) {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(f.bind(this), 1000 * seconds);
    };
    constructor(location: Entity) {
        super(location, new NullClient());
    };
}

export class Room extends Entity {
    reset(): void {
        this.contents = [];
    }
    contentDescription(observer?: Entity) {
        const str = this.contentString(observer);
        return str && 'You notice ' + str + ' here.';
    };
    handleCommand(initiator: Entity, full: string, words: Array<string>): boolean {
        for (let i = 0; i < this.contents.length; i++) {
            if (this.contents[i].handleCommand(initiator, full, words)) {
                return true;
            }
        };
        return super.handleCommand(initiator, full, words);
    };
    emit(eventName, ...args) {
        let hadListeners = false;
        this.contents.filter(entity => entity.repeatLocationEvents).forEach(
            entity => {
                hadListeners = entity.emit(eventName, ...args) || hadListeners;
            });
        return super.emit(eventName, ...args) || hadListeners;
    };
}
