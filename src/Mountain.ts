import { cap, Entity, Body, Room, Item, Go, North, South, East, West, Action, ActionResult, Automaton } from './Narrator';
import Client from './Client';
import { BottomofMountain } from './BottomofMountain';

class Place extends Room {
    desc: string;
    area: Mountain;
    constructor(area: Mountain) {
        super();
        this.area = area;
    };
    description(observer?: Entity) {
        return this.desc + ' ' + this.contentDescription(observer);
    };
}

class Start extends Place {
    reset() {
        this.contents = [
            new North(this, this.area.crevice),
            new South(this, this.area.cliff),
            new East(this, this.area.path),
            new West(this, this.area.rocks),
            new Robin(this)
        ];
    }
    desc: string = 'You are at the peak of a mountain on a clear, sunny day. As you look over a steep cliff to the south, you see a beautiful green valley. To the east a narrow path winds down. To the west are some sharp rocks. North of you is deep snow.';
}

class Path extends Place {
    reset() {
        const east = new East(this, this.area.wooded);
        east.addAction(new PassSpider());
        this.contents = [
            new North(this, this.area.crevice),
            new South(this, this.area.cliff),
            east,
            new West(this, this.area.start),
            new Spider(this)
        ];
        this.desc = 'You are on a narrow path leading east and west. There’s a steep cliff to the south and dangerous-looking deep snow to the north.';
    }
}

class Cliff extends Place {
    desc: string = 'Oh no, that steep cliff with the beautiful view doesn’t seem so beautiful now -- you fell down it!';
}

class Crevice extends Place {
    desc: string = 'The deep snow was hiding an ice crevice. You fall and get stuck in it!';
}

class Robin extends Automaton {
    names = ['robin', 'bird'];
    goes = 'flies';
    leader: Body = null;
    properName = 'a robin';
    description(): string { return 'The robin has an orange breast. Its beak is medium-sized, suitable for both seeds and bugs.'; };
    constructor(location?: Entity) {
        super(location);
        this.on('enter', (act: ActionResult) => {
            if (act.initiator.has(item => item instanceof Seeds)) {
                this.leader = act.initiator as Body;
                this.plan(2, () => this.cmd(`emote turns one eye toward ${this.leader.properName}.`));
            } else {
                this.plan(5, () => this.cmd('emote hops around looking for worms, bugs or seeds.'));
            }
        });
        this.on('go', (act: ActionResult) => {
            if (act.initiator === this.leader &&
                this.leader.has(item => item instanceof Seeds)) {
                this.plan(1, () => this.cmd(`go ${act.receivers[0].names[0]}`));
            }
        });
        this.on('emote', (act: ActionResult) => {
            if (act.initiator instanceof Spider) {
                this.cmd('eat spider');
            }
        });
        this.on('stuck', (act: ActionResult) => {
            if (act.initiator === this.leader &&
                this.location !== this.initialLocation) {
                console.log('emote flies away.');
                this.cmd('emote flies away.');
                this.reset();
            }
        });
    };
}

class Seeds extends Item {
    names = ['seeds'];
    oneLiner() {
        return 'some seeds';
    };
}

class Miss extends Action {
    verb: Array<string> = ['kill', 'hit', 'smash', 'murder'];
    attempt(initiator: Entity, receivers: Array<Entity>): void {
        initiator.location.emit(this.verb[0], {
            initiator, receivers, success: false
        });
    };
}

class Spider extends Automaton {
    goes = 'crawls';
    properName = 'a menacing spider';
    description(): string { return 'The spider\'s menacing fangs seem disproportionate to its small body.'; };
    names = ['spider'];
    constructor(location?: Entity) {
        super(location);
        this.on('enter', (act: ActionResult) => {
            this.plan(5, () => this.cmd('emote bares its fangs.'));
        });
        this.addAction(new Miss());
        this.addAction(new EatSpider());
        this.on('kill', (act: ActionResult) => {
            if (act.receivers[0] === this) {
                if (act.initiator instanceof Body) {
                    this.cmd(`emote nimbly evades ${act.initiator.properName}'s attack.`);
                }
            }
        });
        this.on('pass spider', (act: ActionResult) => {
            if (!act.success) {
                if (act.initiator instanceof Body) {
                    this.cmd(`emote blocks ${act.initiator.properName}'s path.`);
                }
            }
        });
        this.on('eat', (act: ActionResult) => {
            if (!act.success) {
                if (act.initiator instanceof Body && act.receivers[0] === this) {
                    this.cmd(`emote scuttles to the side as ${act.initiator.properName} attempts to eat it.`);
                }
            }
        });
    };
}

class PassSpider extends Go {
    attempt(initiator: Entity, receivers: Array<Entity>): void {
        if (initiator.location.has(it => it instanceof Spider)) {
            initiator.location.emit('pass spider', {
                initiator, receivers, success: false
            });
        } else {
            this.succeed(initiator, receivers);
        }
    };
}

class EatSpider extends Action {
    verb: Array<string> = ['eat'];
    attempt(initiator: Entity, receivers: Array<Entity>): void {
        if (initiator instanceof Robin) {
            this.succeed(initiator, receivers);
        } else {
            initiator.location.emit(this.verb[0], {
                initiator, receivers, success: false
            });
        }
    };
    succeed(initiator: Entity, receivers: Array<Entity>): void {
        if (initiator instanceof Body) {
            initiator.location.emit('message', `${cap(initiator.properName)} eats ${receivers[0].oneLiner()}.`);
        }
        receivers[0].enter(initiator);
        super.succeed(initiator, receivers);
    };
}

class Rocks extends Place {
    reset() {
        this.contents = [
            new East(this, this.area.start),
            new Seeds(this)
        ];
    }
    desc: string = 'Steep, sharp rocks surround you, giving you no choice but to go back east where you came from.';
}

class Wooded extends Place {
    reset() {
        this.contents = [
            new North(this, this.area.bottom.forest),
            new West(this, this.area.path)
        ];
        this.area.bottom.forest.contents.push(
            new South(this.area.bottom.forest, this));
    };
    desc: string = 'You start down the path and see that it is all clear, you hear birds and are undisturbed as you make your way through the wooded path. Next step to the adventure lies just ahead!';
}

export class Mountain {
    bottom: BottomofMountain;
    start: Place;
    path: Place;
    cliff: Place;
    crevice: Place;
    rocks: Place;
    wooded: Place;
    play(client: Client): Body {
        return new Body(this.start, client);
    };
    constructor() {
        this.bottom = new BottomofMountain();
        this.start = new Start(this);
        this.path = new Path(this);
        this.cliff = new Cliff(this);
        this.crevice = new Crevice(this);
        this.rocks = new Rocks(this);
        this.wooded = new Wooded(this);
        Object.keys(this).forEach(key => {
            const member = this[key];
            if (member instanceof Place) {
                member.reset();
            }
        });
    }
}
