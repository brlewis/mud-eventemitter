import Client from './Client';

export default class Yeller {
    client: Client;
    constructor(client: Client) {
        this.client = client;
        client.on('message', (input: string) => client.send(input.toUpperCase()));
    }
}  
