import { FrontDeskArea } from './src/FrontDesk';
import NodeClient from './src/NodeClient';

const desk = new FrontDeskArea();
desk.play(new NodeClient());
